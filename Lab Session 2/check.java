//Program checks if single String input is a consonant or vowel.
//IIT CS115 Beginning Exercises
//RNellis, CS115 TA
import java.util.Scanner;

public class check {
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		String in = scan.nextLine();
		if(in.equalsIgnoreCase("a") || in.equalsIgnoreCase("e") ||
			in.equalsIgnoreCase("i") || in.equalsIgnoreCase("o") ||
			in.equalsIgnoreCase("u")) {
			
			System.out.println("Vowel");
		}
		else
			System.out.println("Consonant");
	}
}