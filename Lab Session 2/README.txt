Exercises for Lab and Assignments

+ Program to Print an Integer Entered by the User (inte.java)
+ Program to Add two Integers and print the result (add.java)
+ Program to Multiply two floating point numbers (floatmult.java)
+ Program to check whether a Character is Vowel or Consonant (check.java)
+ Program to check Leap Year (leap.java)

These programs are very simplistic and not designed to loop or provide extra
output when not necessary.